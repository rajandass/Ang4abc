import { GitciangPage } from './app.po';

describe('gitciang App', () => {
  let page: GitciangPage;

  beforeEach(() => {
    page = new GitciangPage();
  });

  it('should display title app4test', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app4test');
  });

  it('should click 3 times and reset with matching points', () => {
    page.navigateTo();
    expect(page.getPoints()).toBe('1');
    page.getButtonPoints().click();
    page.getButtonPoints().click();
    page.getButtonPoints().click();
    expect(page.getPoints()).toBe('4');

    //page.getButtonReset().click();
    //expect(page.getPoints()).toBe('0');
  });
});
