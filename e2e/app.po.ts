import { browser, by, element } from 'protractor';

export class GitciangPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getPoints(){
  	return element(by.css('app-root span')).getText();
  }

  getButtonPoints(){
  	return element(by.cssContainingText('button','click'));
  }
  getButtonReset(){
  	return element(by.cssContainingText('button','reset'));
  }
 
}
