import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app4test'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app4test');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app4test');
  }));

  it('should increase the point by 1 if button is clicked',async(()=> {
    //create a mock module
    const fixture = TestBed.createComponent(AppComponent);
    // detect the chnages
    fixture.detectChanges();
    //to ge the html element and check
    expect(fixture.componentInstance.points).toBe(1);
    fixture.debugElement.nativeElement.querySelector('button').click();
    expect(fixture.componentInstance.points).toBe(2);

  }));



});
